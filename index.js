import React from 'react';
import { AppRegistry } from 'react-native';
import App from './src/App';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';


// new!!!
import GoToWelcomeSwiper from './src/components/GoToWelcomeSwiper';
import configureStore from './src/config/Store';
import Home from './src/screens/Home';

const store = configureStore();

const RNTurbolinksRedux = () => (
  <Provider store={store}>
      <App />
  </Provider>
);

AppRegistry.registerComponent('RNTurbolinks', () => RNTurbolinksRedux);
AppRegistry.registerComponent('Home', () => Home);
//AppRegistry.registerComponent('AuthenticationView', () => AuthenticationView);
//AppRegistry.registerComponent('WelcomeSwiper', () => WelcomeSwiper);
//AppRegistry.registerComponent('Auth', () => Auth);
//AppRegistry.registerComponent('Test', () => Test);
//AppRegistry.registerComponent('GoToWelcomeSwiper', () => GoToWelcomeSwiper);
