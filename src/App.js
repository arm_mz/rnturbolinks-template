import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { AppRegistry } from 'react-native';

import { connect } from 'react-redux';
import Turbolinks from 'react-native-turbolinks';
import { createStackNavigator } from 'react-navigation';

import Start from './screens/Start';

class App extends React.Component {
  render() {
    console.log('CURRENT STATE: ', this.props.currentState);
    return (
      <Start />
    );
  }
}

const mapStateToProps = state => {
  return {
    currentState: state.auth,
    authenticated: state.auth.authenticated
  }
}

export default connect(mapStateToProps)(App);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});


AppRegistry.registerComponent('Start', () => Start);
