import { combineReducers } from 'redux';

import AuthenticationReducer from './AuthenticationReducer';

const rootReducer = combineReducers({
  auth: AuthenticationReducer   /* We have an auth property*/
});

export default rootReducer;
