const initialState = {
  authenticated: false,
  clickInLogIn: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        authenticated: action.payload
      };
    default:
      return state;
  }
}

export default reducer;
