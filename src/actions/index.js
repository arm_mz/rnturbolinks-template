const confirmLogin = () => {
  return (dispatch) => {
    dispatch({ type: 'LOGIN_SUCCESS', payload: true});
  }
};

export { confirmLogin };
