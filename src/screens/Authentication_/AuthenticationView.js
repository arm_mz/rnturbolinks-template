import React, { Component } from 'react';
import { WebView, StatusBar, View, StyleSheet } from 'react-native';

import { connect } from 'react-redux'
import Turbolinks from 'react-native-turbolinks';

import Ionicons from 'react-native-vector-icons/Ionicons'
import Icon from 'react-native-vector-icons/FontAwesome';

import Constants from '../../config/Constants';
//import login from '../../actions/';

const signInURL = Constants.baseURL + '/users/sign_in'

class AuthenticationView extends Component {
  handleLoadStart = e => {
    console.log('native.Event.url ... [' +  e.nativeEvent.url + ']')
    if (e.nativeEvent.url == signInURL) {
      console.log('Entering sign_in page...');
      return true;
    }
    else {
      console.log('Entering tabBasedDashboard!!!')
      console.log('native.Event.url ... [' +  e.nativeEvent.url + ']')
      setTimeout(() => { StatusBar.setBarStyle=('default')}, 800);
      setTimeout(() => { Turbolinks.reloadSession()}, 900);
      Turbolinks.dismiss();
      Turbolinks.startTabBasedApp([
        {component: 'Test', tabTitle: 'Test 1', navBarHidden: true},
        {component: 'Test', tabTitle: 'Test 2', navBarHidden: true}
      ])
      return false;
    }
  }

  closeModal() {
    alert('Goodbye!!!');
    Turbolinks.dismiss();
  }

  render() {
    return (
      <View style={styles.webContainer} >
        <View style={styles.closeSection}>
          <View style={{ marginRight: 20}}>
            <Ionicons
              name='ios-close'
              size={40}
              onPress={this.closeModal}
              iconStyle={{
                margin: 20
              }}
            />
          </View>
        </View>
        <WebView
          style={{backgroundColor: 'transparent'}}
          source={{uri: signInURL}}
          onLoadStart={this.handleLoadStart}
          contentInset={{
            top: 80
          }}
          scrollEnabled={false}
        />
      </View>
    );
  }
}

//export default connect()(AuthenticationView);
export default AuthenticationView;

const mapStateToProps = state => {
  return {
    authenticated: state.auth.authenticated
  }
}

const styles = StyleSheet.create({
  webContainer: {
    flex: 1
  },
  closeSection: {
    flexDirection: 'row',
    width: '100%',
    height: '10%',
    //backgroundColor: 'gray',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  }
});
