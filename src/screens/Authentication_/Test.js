import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';

class Test extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>This is a test screen</Text>
      </View>
    );
  }
}

export default Test;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center'
  }
})
