import React, { Component } from 'react';
import { WebView, StatusBar, View, StyleSheet } from 'react-native';

import { connect } from 'react-redux'
import Turbolinks from 'react-native-turbolinks';

import Ionicons from 'react-native-vector-icons/Ionicons';

import Constants from '../../config/Constants';
import login from '../../actions/';

const signUpURL = Constants.baseURL + '/users/sign_up'

export default class SignUpView extends Component {
  componentDidMount() = async() => {
    this.setState({closeIcon: await Icon.getImageSource('close')})
  }
  handleLoadStart = e => {
    console.log('native.Event.url ... [' +  e.nativeEvent.url + ']')
    if (e.nativeEvent.url == signInURL) {
      console.log('Entering sign_up page...');
      return true;
    }
    else {
      //{login()}
      console.log('Entering tabBasedDashboard!!!')
      console.log('native.Event.url ... [' +  e.nativeEvent.url + ']')
      setTimeout(() => { StatusBar.setBarStyle=('default')}, 800);
      setTimeout(() => { Turbolinks.reloadSession()}, 900);
      Turbolinks.dismiss();
      Turbolinks.startTabBasedApp([
        {component: 'Test', tabTitle: 'Test 1', navBarHidden: true},
        {component: 'Test', tabTitle: 'Test 2', navBarHidden: true}
      ])
      return false;
    }
  }

  render() {
    return (
      <View style={styles.webContainer} >
        <View style={closeSection}>
          <Ionicons
            name='close'
            size={20}
          />
        </View>
        <WebView
          style={{backgroundColor: 'transparent'}}
          source={{uri: signInURL}}
          onLoadStart={this.handleLoadStart}
          contentInset={{
            top: 80
          }}
          scrollEnabled={false}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    authenticated: state.auth.authenticated
  }
}

//export default connect(mapStateToProps, { login })(AuthenticationView);

const styles = StyleSheet.create({
  webContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  closeSection: {
    width: '100%',
    height: '20%'
  }
});
