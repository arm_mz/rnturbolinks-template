import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Alert } from 'react-native';

import { connect } from 'react-redux';

import { Button } from 'react-native-elements';
import Turbolinks from 'react-native-turbolinks';

import Ionicons from 'react-native-vector-icons/Ionicons';

import Constants from '../../config/Constants';

class Auth extends Component {
  onLogInPress = () => {
//this.setState(previousState => {
//      return { closeIcon: Ionicons.getImageSource('close')}
//    })
    Turbolinks.startSingleScreenApp({component: 'AuthenticationView', modal: true, rightButtonIcon: this.state.closeIcon});

  }

  onSignUpPress = () => {
    //Turbolinks.visit({url: Constants.baseURL + '/users/sign_up', modal: true, title: 'Sign Up'})
  }

//  onLoginPress = () => {
//    this.props.navigator.showModal({
//      screen: 'actiun.LogInScreen'
//    })
//  }

//  onSignUpPress = () => {
//    this.props.navigator.showModal({
//      screen: 'actiun.SignUpScreen',
//      animationType: 'slide-up'
//    })
//  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.imageContent}>
          <Image
            resizeMode={'contain'}
            style={styles.defaultSize}
            source={require('../../assets/white-actiun-logo.png')}
          />
        </View>
        <View style={styles.groupBtnSection}>
          <Button
            onPress={this.onLogInPress}
            // onPress={this.props.callfeat}
            title='INGRESAR'
            titleStyle={{ fontWeight: '700' }}
            buttonStyle={{
              backgroundColor: '#2980B9',
              width: '100%',
              borderColor: 'transparent',
              borderWidth: 0,
              borderRadius: 5,
              paddingTop: 5,
              paddingBottom: 5
            }}
          />
          <Button
            onPress={this.onSignUpPress}
            title='CREAR CUENTA'
            titleStyle={{ fontWeight: '700' }}
            buttonStyle={{
              backgroundColor: '#2ECC71',
              width: '100%',
              borderColor: 'transparent',
              borderWidth: 0,
              borderRadius: 5,
              paddingTop: 5,
              paddingBottom: 5
            }}
            // margin between buttons
            containerStyle={{
              marginTop: 15,
              marginBottom: 50
            }}
          />
        </View>
      </View>
    );
  }
}

export default connect()(Auth);
// export default Auth;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: '#3C96D2'
  },
  imageContent: {
    justifyContent: 'flex-end',
    width: '70.5999%',
    height: '50%',
  },
  defaultSize: {
    width: '100%',
    height: '40%',
  },
  groupBtnSection: {
    justifyContent: 'flex-end',
    width: '70.199%',
    height: '50%',
  }
});
