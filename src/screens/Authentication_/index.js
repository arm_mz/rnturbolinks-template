import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

//import { Button } from 'react-native-elements';
import Swiper from 'react-native-swiper';
import { connect } from 'react-redux';

import Authentication from './Auth';
import Info from '../../components/Info';

class WelcomeSwiper extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }
  
  render() {
    const content_one = `Dolore voluptate aute capicola shankle drumstick.
    Shankle chicken porchetta, laborum lorem beef cupim adipisicing dolore.`;

    const content_two = `Flank ad ham, ut beef turducken enim magna.`;
    const header_one = 'Spicy jalapeno';
    const header_two = 'Kevin excepteur porchetta meatball'
    return (
      <View style={styles.container}>
        <Swiper style={styles.wrapper} loop={false} activeDotColor='white'>
          <View style={styles.slide}>
            <Authentication navigator={this.props.navigator} callfeat={this.props.tlinks}/>
          </View>
          <View style={styles.slide}>
            <Info header={header_one} content={content_one}/>
          </View>
          <View stye={styles.slide}>  /* Not really necessary */
            <Info header={header_two} content={content_two}/>
          </View>
          <View style={styles.slide}>
            <Authentication navigator={this.props.navigator}/>
          </View>
        </Swiper>
      </View>
    );
  }
}

export default connect(null, null)(WelcomeSwiper);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {
  },
  slide: {
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: '#3C96D2'
  },
});
