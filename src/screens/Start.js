import React from 'react';
import { StyleSheet, Text, View, WebView } from 'react-native';

import { connect } from 'react-redux';
import Turbolinks from 'react-native-turbolinks';
import { createStackNavigator } from 'react-navigation';
import { confirmLogin } from '../actions/';

const SIGN_IN_URL = 'http://localhost:3000/users-sign_in';
const HOME_URL = 'http://localhost:3000/'

class Start extends React.Component {
  _onLoadStart = (e) => {
    if (e.nativeEvent.title == 'Dummyapp' || this.props.authenticated) {
      console.log('Welcome home!!!');
      this.props.confirmLoginSuccess();
      Turbolinks.dismiss();
      //Turbolinks.startSingleScreenApp({component: 'Home'});
    }
  }

  _success() {

  }

  render() {
    console.log('CURRENT STATE FROM START SCREEN', this.props.currentState);
    return (
      <WebView
        source={{uri: 'http://localhost:3000/'}}
        onLoadStart={this._onLoadStart}
      />
    );
  }
 }


const mapStateToProps = state => {
  return {
    authenticated: state.auth.authenticated,
    currentState: state.auth
  }
}

const mapDispatchToProps = {
  confirmLoginSuccess: () => confirmLogin(),
}


export default connect(mapStateToProps, mapDispatchToProps)(Start);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
