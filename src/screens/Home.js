import React from 'react';
import { StyleSheet, Text, View, WebView } from 'react-native';

import Turbolinks from 'react-native-turbolinks';

const SIGN_IN_URL = 'http://localhost:3000/users/sign_in';

class Home extends React.Component {
  _onLoadStart = e => {
    if (e.nativeEvent.url == SIGN_IN_URL) return true;
    setTimeout(() => {}, 800);
    setTimeout(() => { Turbolinks.reloadSession() }, 900);
    Turbolinks.startSingleScreenApp({component: 'Home'});
  }

  render() {
    return (
      <WebView
        source={{uri: 'http://localhost:3000/'}}
        onLoadStart={this._onLoadStart}
      />
    );
  }
 }

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
