import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

class Info extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.imageContent}>
          <Image
            resizeMode={'contain'}
            style={styles.defaultSize}
            source={require('../assets/actiun_icon_white.png')}
          />
        </View>
        <View style={styles.contentSection}>
          <Text style={styles.header}>
            {this.props.header}
          </Text>
          <Text style={styles.description}>
            {this.props.content}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: '#3C96D2'
  },
  imageContent: {
    justifyContent: 'flex-end',
    width: '70.5999%',
    height: '50%',
  },
  defaultSize: {
    width: '100%',
    height: '40%',
  },
  contentSection: {
    justifyContent: 'flex-start',
    marginTop: 35,
    width: '70.199%',
    height: '50%',
  },
  header: {
    color: 'white',
    fontSize: 25,
    fontStyle: 'normal',
    letterSpacing: 1,
    textAlign: 'center'
  },
  description: {
    color: 'white',
    fontSize: 15,
    fontFamily: 'Arial',
    letterSpacing: 1,
    textAlign: 'center',
    marginTop: 15
  }
});

export default Info;
