import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { Button } from 'react-native-elements';
import { connect } from 'react-redux';

import WelcomeSwiper from '../screens/Authentication/';
import GoToWelcomeSwiper from './GoToWelcomeSwiper';

import login from '../actions/';

class EntryPoint extends Component {
  dispatchLogin() {
    console.log('clicking...')
  }

  render() {
    console.log('AUTHENTICATED STATUS: ', this.props.authenticated);
    if ( this.props.authenticated == false ) {
      return (
        <GoToWelcomeSwiper f={this.dispatchLogin} />
      );
    } else {
      return (
        <View style={styles.container}>
          <Button
            title='Button'
            onPress={this.dispatchLogin}
          />
        </View>
      );
    }
  }
}

const mapStateToProps = state => {
  return {
    authenticated: state.auth.authenticated
  }
}

export default connect(mapStateToProps, null)(EntryPoint);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'grey'
  }
});
