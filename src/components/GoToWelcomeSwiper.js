import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';

import { Button } from 'react-native-elements';
import Turbolinks from 'react-native-turbolinks';

import { connect } from 'react-redux';

//import WelcomeSwiper from '../screens/Authentication/';
//import AuthenticationView from '../screens/Authentication/AuthenticationView';

class GoToWelcomeSwiper extends Component {
  goToLogin() {
    Turbolinks.startSingleScreenApp({component: 'AuthenticationView', modal: true})
  }

  render() {
    console.log(this.props.f);
    return (
      <WelcomeSwiper tlinks={this.goToLogin}/>
    );
  }
}

export default connect(null, null)(GoToWelcomeSwiper);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'orange'
  }
});
